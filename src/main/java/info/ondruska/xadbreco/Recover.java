package info.ondruska.xadbreco;

import com.google.common.primitives.Ints;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.sql.XAConnection;
import javax.sql.XADataSource;
import javax.transaction.xa.XAException;
import javax.transaction.xa.XAResource;
import javax.transaction.xa.Xid;
import org.apache.derby.jdbc.ClientXADataSource;
import org.apache.derby.jdbc.EmbeddedXADataSource;

/**
 * Remove obsolete lock records caused by not gracefully removing database that
 * was under transaction manager control. This can be observed by having records
 * in TRANSACTION_TABLE (and related in LOCK_TABLE) with state
 * PREPARED:<br>SELECT * FROM SYSCS_DIAG.LOCK_TABLE;<br> SELECT * FROM
 * SYSCS_DIAG.TRANSACTION_TABLE;
 *
 * @author Knut Anders Hatlen, Peter Ondruška (just slightly modified)
 */
public class Recover {

  private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(Recover.class.getName());

  private static void recover(XADataSource xads) {

    try {

      XAConnection xac = xads.getXAConnection();
      XAResource xar = xac.getXAResource();

      for (Xid xid : xar.recover(XAResource.TMSTARTRSCAN)) {
        xar.rollback(xid);
        LOGGER.log(Level.INFO, "Recovered Xid {0} using rollback", xid.toString());
      }

      xac.close();

    } catch (SQLException | XAException e) {
      LOGGER.log(Level.WARNING, null, e);
    }

  }

  private static void client(String host, Integer port, String db, String user, String password) {

    ClientXADataSource ds = new ClientXADataSource();
    ds.setDatabaseName(db);
    ds.setUser(user);
    ds.setPassword(password);
    ds.setServerName(host);
    if (port != null) {
      ds.setPortNumber(port);
    }

    recover(ds);

  }

  private static void embedded(String db, String user, String password) {

    EmbeddedXADataSource ds = new EmbeddedXADataSource();
    ds.setDatabaseName(db);
    ds.setUser(user);
    ds.setPassword(password);

    recover(ds);

    // for embedded proceed with shutdown
    try {
      ds.setShutdownDatabase("shutdown");
      ds.getConnection();
    } catch (SQLException e) {
      // ignore expected response
      if (!"08006".equals(e.getSQLState())) {
        LOGGER.log(Level.SEVERE, null, e);
      }
    }

  }

  public static void main(String[] args) {

    int i = 0;
    switch (args.length) {
      case 3:
        embedded(args[i++], args[i++], args[i++]);
        break;
      case 5:
        client(args[i++], Ints.tryParse(args[i++]), args[i++], args[i++], args[i++]);
        break;
      default:
        System.err.println("Expecting 3 (database, user, password) or 5 (host, port, database, user, password) parameters to Derby database to recover (rollback) XA transactions");
        break;
    }

  }

}
